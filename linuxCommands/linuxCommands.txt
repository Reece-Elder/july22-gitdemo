Variables in Linux 
- Makes life easier, a really long path directory = simple variable that you can use and reuse 
- password = password123! / password = ${PASSWORD} Not hard coding confidential information into your linux 

variableName=variableValue

to access the value of a variable ${variable name}

By default variables are only accessible by this context, not usable by child processes (printenv). If we want to make them available we must export the variable 

export newVariable=newValue
export newVariable

set - prints out all shell variables
env - prints out all exported variables (environment variables)

grep "search term" <file> - Search within this file to find this content

| - connect two commands together, standardout (stdout) is plugged into the standardin (stdin) of the next thing

env | grep "search term" - search within the env out to find our data

env | less - Opens the file, you can then search through it and find what you want with a search term


Linux Scripting
Create a script of bash commands to do our linux proceses for us
Scripts are files that end in .sh and just list our commands

shebang - sits at the top of the file to specify how to run this script
#! /bin/bash

Run a script using ./<script name> 


Scripting Exercise
export favCol=Purple
export favPizza=Hawaaian

touch scriptExercise.sh
#! /bin/bash
# Print out my env var
echo ${favCol}

echo ${favPizza} > favPizza.txt

cat favPizza.txt
